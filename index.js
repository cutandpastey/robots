var GameWorld = require('./lib/GameWorld');
var Robot = require('./lib/Robot');

var world = new GameWorld(5, 3);

var robot1 = new Robot([1, 1], 'E', 'RFRFRFRF', 'robot1', true);
var robot2 = new Robot([3, 2], 'N', 'FRRFLLFFRRFLL', 'robot2', true);
var robot3 = new Robot([0, 3], 'W', 'LLFFFLFLFL', 'robot3', true);


/*

If the Robots are added to the world in parallel the test fails.
This is because it takes Robot 2 more moves to get lost than robot 3.
The sample output assumes robot 2 gets lost before we add robot 3


world.injectGameObject(robot2);
world.injectGameObject(robot3);

*/

var robots = [robot2, robot3];

world.emitter.subscribe('gameobject:destroyed', function() {
  var nextRobot = robots.splice(0, 1)[0];
  
  if (!nextRobot) {
    console.log('COMPLETED!!');
    process.exit(0);
  }

  world.injectGameObject(nextRobot);
});

world.injectGameObject(robot1);
