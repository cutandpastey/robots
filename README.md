# Mars Landings

This is a simple simulation where tiny robots land on Mars' surface.

`npm start` will run the simulation

`npm test` will run a mocha unit test asserting the robots coordinates are correct after recieving a set of instructions

dont forget to install the deps with `npm install`.


##Todo

- Unit test individual components such as GameWorld and Robot
- Abstract some Gameworld functionality and add a Mars sub class for more specific 'red planet' functionality
- Abstract GameObject out of Robot for a more generic feature set which can be extended, you might want to send people to mars one day.
