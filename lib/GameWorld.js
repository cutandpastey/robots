var FRAME_SPEED = 17;

var _ = require('lodash');
var Mediator = require('mediator-js').Mediator;
var errorHandle = require('./errorHandler');

var GameWorld = function(horizontalBound, verticalBound) {

  if (verticalBound && !_.isNumber(verticalBound)) {
    errorHandle('Only a number can be used as a game worlds vertical bound, you used: %s', verticalBound);
  }

  if (horizontalBound && !_.isNumber(horizontalBound)) {
    errorHandle('Only a number can be used as a game worlds horizontalBound bound, you used: %s', horizontalBound);
  }

  if(verticalBound > 50){
    errorHandle('A game world can have a maximum height of 50, you passed: %s', verticalBound);
  }
  
  if(horizontalBound > 50){
    errorHandle('A game world can have a maximum wiidth of 50, you passed: %s', horizontalBound);
  }

  this.verticalBound = (verticalBound || 50);
  this.horizontalBound = (horizontalBound || 50);

  this.init();
};

GameWorld.prototype = {

  interval: null,

  orientations: {
    N: [0, 1],
    E: [1, 0],
    S: [0, -1],
    W: [-1, 0]
  },

  verticalBound: 50,
  horizontalBound: 50,

  gameObjects: [],

  //initialises the game world
  init: function() {
    var emitter = this.emitter = new Mediator();

    this.interval = setInterval(function() {

      emitter.publish('tick');
      this.checkGameObjectPositions();

    }.bind(this), FRAME_SPEED);

  },

  //passes world specific params to the game objects being added to the world
  //this encapsulates the world 
  injectGameObject: function injectGameObject(gameObject) {
    
    if (!_.isFunction(gameObject.addToGameWorld)) {
      errorHandle('Expected a game objetc to be passed, istead got: %s', gameObject);
    }
    //
    else {
      this.gameObjects.push(gameObject);
      gameObject.addToGameWorld(this.emitter, this.orientations);
    }
  },

  //function checkGameObjectPositions
  // - Checks that game objects are still within the world after an update occurred
  checkGameObjectPositions: function() {

    this.gameObjects.forEach(function(gameObject, index) {

      //check the bounds of each robot
      if ((gameObject.x > this.horizontalBound) || (gameObject.x <= -1) || (gameObject.y > this.verticalBound) || (gameObject.y <= -1)) {
        //remove the robot from the active game objects and shut him down
        this.gameObjects.splice(index, 1)[0].shutDown();
      }

    }, this);
  }

};

module.exports = GameWorld;
