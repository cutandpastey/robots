var util = require('util');

//function ErrorHandle
// - takes a string which is then formatted to provide easier debugging
module.exports = function (errorString, object){
  
  //this should be more dynamic, ie it should accept multiple objects to format
  errorString = util.format(errorString, object);
  
  //in production you will probably want a hook for logging errors somewhere
  
  //emitter.emit('application:error', errorString);

  throw new Error(errorString);
  
};
