var _ = require('lodash');
var errorHandel = require('./errorHandler');

var robotScentPositions = [];

module.exports = {

  //function setScentPosition
  // - adds a new position to the known scent positions
  setScentPosition: function(position) {

    if (!_.isArray(position)) {
      errorHandel('Only a valid vecotr can be set as a robot scent position.  You set: %3', position);
    }

    robotScentPositions.push(position);

  },

  //function getScentPosition
  // - returns a known position at a given index
  getScentPosition: function(index) {
    return (robotScentPositions[index] || null);
  },

  //function checkScentPositions
  // - loops through all known scent positions and 
  //   checks that the intended position is not a known location
  checkScentPositions: function(position) {
    //assume we don't know this position by default
    var isKnowScentPosition = false;

    // loop through all know scent positions
    robotScentPositions.forEach(function(knownPosition) {

      //if the positions are the same
      if ((knownPosition[0] === position[0]) && (knownPosition[1] === position[1])) {
        isKnowScentPosition = true;
      }

    });

    return isKnowScentPosition;
  }

};
