var _ = require('lodash');
var errorHandle = require('./errorHandler');
var robotMemory = require('./robotMemory');

var Robot = function(startPosition, orientation, commands, name, shouldDebug) {

  /* set Starting position */

  if (!_.isArray(startPosition)) {
    // At this point its arguable that we should have a default of [0, 0] but I think its better to be clear. 
    // If another developer picked this up and didn't know about the assumed defaults of [0, 0] you could get into trouble.
    errorHandle('A valid start position must be passed to a Robot instance, you passed: %s', startPosition);
  }

  else if (startPosition[0] > 50) {
    errorHandle('A robot may be given a maximum vertical start point of 50, you passed: %s', startPosition[0]);
  }

  else if (startPosition[1] > 50) {
    errorHandle('A robot may be given a maximum horizontal start point of 50, you passed: %s', startPosition[1]);
  }

  else {
    this.x = startPosition[0];
    this.y = startPosition[1];
    this.orientation = startPosition[2];
  }


  /* set Starting Orientation */
  
  if (!orientation || !_.isString(orientation)) {
    errorHandle('A valid orientation value MUST be passed to a new instance of a Robot, you passed: %s', orientation);
  } 
  
  else {
    this.orientation = orientation;
  }

  /* Transform and store a given set of commands */

  //if the commands passed are a string like 'LRFFLFRLF'
  if (_.isString(commands)) {
    this.commands = commands.split('');
  }

  //if the commands is an array like [ 'L', 'R', 'F', 'F', 'L', 'F', 'R', 'L', 'F' ]
  else if (_.isArray(commands)) {
    this.commands = commands;
  }

  else if(commands.length > 100){
    errorHandle('A maximum of 100 commands can be given to a Robot, you passed: %s', commands.length);
  }

  //else throw an error
  else {
    errorHandle('Valid commands must be passed to a robot on initialisation, you passed: %s', commands);
  }

  //set a robots name, useful for debugging
  this.name = (name || '');
  this.shouldDebug = (shouldDebug || false);
};

Robot.prototype = {

  commands: null,
  emitter: null,
  x: 0,
  y: 0,
  orientation: 'N',
  isLost: false,

  //function addRobotToGameWorld
  // - called when the robot is added to a game world instance
  // - It should be assumed that this lives within the interface of all game objects, as such it maybe should be extracted
  //   As we only use robots in this case we can leave it in this module
  addToGameWorld: function addRobotToGameWorld(emitter, orientations) {
    this.emitter = emitter;
    this.orientations = orientations;
    this.subscription = this.emitter.subscribe('tick', this.tick, {}, this);
  },

  //function tick
  // - processes the next command
  tick: function robotTick() {

    //if we have run out of commands to process we have finished so we can tell the application where we are
    if (this.commands.length <= 0) {
      this.emitter.remove('tick', this.subscription.id);
      this.sayFinalLastWords();
      return;
    }

    var command = this.commands.splice(0, 1)[0];
    this.processCommand(command);
  },

  processCommand: function robotProcessCommand(command) {
    switch (command) {
      case 'L':
        this.setOrientation(-1);
        break;

      case 'R':
        this.setOrientation(1);
        break;

      case 'F':
        this.moveForwards();
        break;
    }

  },

  //function getOrientation
  // - sets the new orientation based on the last orientation 
  setOrientation: function(indexModifier) {

    //define all possible orientations
    var possibleDirections = Object.keys(this.orientations);

    //get the current index
    var currentOrientationPosition = possibleDirections.indexOf(this.orientation);

    //get the next index
    var nextOrientationPosition = currentOrientationPosition + indexModifier;

    //reset the index if we need to
    if (nextOrientationPosition >= possibleDirections.length) {
      nextOrientationPosition = 0;
    } else if (nextOrientationPosition < 0) {
      nextOrientationPosition = possibleDirections.length - 1;
    }

    //get the new orientation
    var newOrientation = possibleDirections[nextOrientationPosition];

    //set it
    //when I'm building websites I try to be a lot more functional than this
    //however, in the past, when I've built games they have tended to be much more OOP constructs
    this.orientation = newOrientation;

  },

  //function moveForwards
  moveForwards: function() {
    // get the vector to transform our current position 
    var vector = this.orientations[this.orientation];

    //workout where we would like to go
    var intendedPosition = [(this.x + vector[0]), (this.y + vector[1])];

    //check if this position is marked
    if (robotMemory.checkScentPositions(intendedPosition)) {
      return;
    }

    //if this position is safe move to it;
    //again, this could be much more functional
    this.x = intendedPosition[0];
    this.y = intendedPosition[1];

  },

  //function shutDown
  // - Called when an object falls off the map
  shutDown: function() {
    //remember the current position
    robotMemory.setScentPosition([this.x, this.y]);

    //stop updating
    this.emitter.remove('tick', this.subscription.id);

    this.isLost = true;

    this.sayFinalLastWords();
  },

  sayFinalLastWords: function() {

    if (this.shouldDebug) {
      console.log('----------------------');
      console.log('%s is done', this.name);
      console.log('My X position is %s', this.x);
      console.log('My Y position is %s', this.y);
      console.log('My orientation position is %s', this.orientation);
      if (this.isLost) {
        console.log('Unfortunatly I did get lost');
      } else {
        console.log('I didn\'t get lost, w00p w00p!');
      }
      console.log('----------------------');
    }

    this.emitter.publish('gameobject:destroyed');
  }

};

module.exports = Robot;
