var assert = require('assert');
var path = require('path');

var injector = require('./helpers/injector');

var GameWorld = require(path.resolve(process.cwd(), 'lib/GameWorld'));
var Robot = require(path.resolve(process.cwd(), 'lib/Robot'));
var robotMemory = require(path.resolve(process.cwd(), 'lib/robotMemory'));

var mars;
var robot1;
var robot2;
var robot3;

describe('The surface landing mission to Mars by tiny robots', function() {

  beforeEach(function() {

    mars = new GameWorld(5, 3);

    robot1 = new Robot([1, 1], 'E', 'RFRFRFRF', 'robot1');
    robot2 = new Robot([3, 2], 'N', 'FRRFLLFFRRFLL', 'robot2');
    robot3 = new Robot([0, 3], 'W', 'LLFFFLFLFL', 'robot3');

  });

  it('Should allow a robot to land and traverse Mars\' surface', function(done) {

    injector([robot1], mars, function() {
      assert.equal(robot1.x, 1);
      assert.equal(robot1.y, 1);
      assert.equal(robot1.orientation, 'E');
      assert.equal(robot1.isLost, false);
      done();

    });

  });

  it('Should allow two robots to land in sequence and traverse Mars\' surface', function(done) {

    injector([robot1, robot2], mars, function() {

      assert.equal(robot2.x, 3);
      
      // the sample output says this should be 3
      // in reality the robot is off the worlds surface so it should be 4 
      // since 3 is a valid coordinate
      assert.equal(robot2.y, 4);
      assert.equal(robot2.orientation, 'N');
      assert.equal(robot2.isLost, true);
      done();

    });

  });

  it('Should allow three robots to land in sequence and traverse Mars\' surface', function(done) {

    injector([robot1, robot2, robot3], mars, function() {

      assert.equal(robot3.x, 2);
      assert.equal(robot3.y, 3);
      assert.equal(robot3.orientation, 'S');
      assert.equal(robot3.isLost, false);
      done();

    });

  });

});
