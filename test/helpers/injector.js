module.exports = function(objects, world, done) {

  var subscription = world.emitter.subscribe('gameobject:destroyed', function() {

    if (objects.length >= 1) {
      world.injectGameObject(objects.splice(0, 1)[0]);
    } 
    
    else {
      world.emitter.remove('gameobject:destroyed', subscription.id);
      done();
    }

  });

  world.injectGameObject(objects.splice(0, 1)[0]);

};
